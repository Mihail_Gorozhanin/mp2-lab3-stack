#include "tsimplestack.h"
#include <gtest/gtest.h>

TEST(TSimpleStack, cant_create_stack_with_negative_size)
{
	ASSERT_ANY_THROW(TSimpleStack<int>(-1));
}

TEST(TSimpleStack, can_create_stack_with_positive_length)
{
	ASSERT_NO_THROW(TSimpleStack<int>(10));
}

TEST(TSimpleStack, can_create_copied_stack)
{
	TSimpleStack<int> st1(2);

	ASSERT_NO_THROW(TSimpleStack<int> st2 = st1);
}

TEST(TSimpleStack, copied_stack_is_equal_to_source_one)
{
	TSimpleStack<int> st1(2);

	st1.put(1);
	st1.put(2);

	TSimpleStack<int> st2 = st1;

	EXPECT_EQ(1, (st2.get() == 2) && (st2.get() == 1));
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
{
	TSimpleStack<int> st1(2);

	st1.put(1);
	st1.put(2);

	TSimpleStack<int> st2 = st1;

	st1.get();

	EXPECT_EQ(2, st2.get());
}

TEST(TSimpleStack, can_put_element_when_stack_isnt_full)
{
	TSimpleStack<int> st1(2);

	ASSERT_NO_THROW(st1.put(1));
}

TEST(TSimpleStack, cant_put_element_when_stack_is_full)
{
	TSimpleStack<int> st1(2);

	st1.put(1);
	st1.put(2);

	ASSERT_ANY_THROW(st1.put(3));
}

TEST(TSimpleStack, can_get_element_when_stack_isnt_empty)
{
	TSimpleStack<int> st(2);

	st.put(2);

	ASSERT_NO_THROW(st.get());
}

TEST(TSimpleStack, cant_get_element_when_stack_is_empty)
{
	TSimpleStack<int> st(2);

	ASSERT_ANY_THROW(st.get());
}

TEST(TSimpleStack, put_and_get_correctly)
{
	TSimpleStack<int> st(1);

	st.put(1);

	EXPECT_EQ(1, st.get());
}